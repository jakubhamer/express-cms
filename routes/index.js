var express = require('express');
var router = express.Router();
const dotenv = require("dotenv")
dotenv.config()
const mongodb = require('mongodb');
const uri = `mongodb+srv://${process.env.IPSUM}:${process.env.LOREM}@cluster0.x5prw.mongodb.net/cms?retryWrites=true&w=majority`;
const client = new mongodb.MongoClient(uri, { useNewUrlParser: true, useUnifiedTopology: true });


/* GET home page. */
router.get('/', function (req, res, next) {
  client.connect(err => {
    const posts_collection = client.db("cms").collection("posts");
    var mysort = { date: -1 };
    posts_collection.find({}).sort(mysort).toArray(function (err, result) {
      if (err) throw err;
      res.render('index', { posts: result });
    });
  });
});

/* Create text post. */
router.post('/text/create', function (req, res) {
  client.connect(err => {
    const posts_collection = client.db("cms").collection("posts");
    posts_collection.insertOne({
      body: req.body.body,
      title: req.body.title,
      author: req.body.author,
      tags: req.body.tags,
      date: Date()
    }, function (err, obj) {
      if (err) throw err;
      res.status(200).redirect('/');
    });
  });
});

/* Delete text post. */
router.post('/text/delete/:textId', function (req, res) {
  deleteText(req, res);
});

function deleteText(req, res) {
  client.connect(err => {
    const posts_collection = client.db("cms").collection("posts");
    var myquery = { _id: new mongodb.ObjectID(req.params.textId) };
    posts_collection.deleteOne(myquery, function (err, obj) {
      if (err) throw err;
      res.status(200).redirect('/');
    });
  });
}
/* Edit text get. */
router.get('/text/edit/:textId', function (req, res) {
  client.connect(err => {
    const posts_collection = client.db("cms").collection("posts");
    var myquery = { _id: new mongodb.ObjectID(req.params.textId) };
    posts_collection.find(myquery).limit(1).toArray(function (err, result) {
      if (err) throw err;
      res.render('edit', { post: result });
    });
  });
});

/* Update text post. */
router.post('/text/update/:textId', function (req, res) {
  client.connect(err => {
    const posts_collection = client.db("cms").collection("posts");
    var myquery = { _id: new mongodb.ObjectID(req.params.textId) };
    var newvalues = { $set: { title: req.body.title, body: req.body.body, date: Date(), tags: req.body.tags } };
    posts_collection.updateOne(myquery, newvalues, function (err, result) {
      if (err) throw err;
      res.status(200).redirect('/');
    });
  });
});

/* Create text post. */
router.post('/contact/add', function (req, res) {
  client.connect(err => {
    const posts_collection = client.db("cms").collection("questions");
    posts_collection.insertOne({
      question: req.body.question,
      name: req.body.name,
      date: Date()
    }, function (err, obj) {
      if (err) throw err;
      res.status(200).redirect('/');
    });
  });
});

module.exports = router;